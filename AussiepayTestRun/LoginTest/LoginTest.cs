﻿using AussiepayTest;
using AussiepayTest.Login;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginTest
{
    public class LoginTest : Base
    {
        private string Email = "support@epayroll.com.au";
        private string Password = "WinterIsComing";
        private string WrongEmail = "something@nothing.com.au";
        private string WrongPassword = "Nostringattached";
        private string ResetEmail = "Charlotte.INGOLD+191074@aussiepay.com.au";

          [Test]
        [Obsolete]
        public void CanLoginToSystemUsingValidCredentials()
        {
            //Act
            Login._Login(Email, Password);

            // Assert 
            Assert.AreEqual(LoginValidation.Title, "Home");

        }

        [Test]
        [Obsolete]
        public void CanNotLoginToSystemUsingInValidCredentials()
        {
            //Act
            Login._Login(WrongEmail, WrongPassword);

            // Assert 
            Assert.AreEqual(LoginValidation.Warning, "Login failed.");

        }


        [Test]
        [Obsolete]
        public void CanSendTheResetPasswordLink()
        {
            //Act
            Login.ResetPassword(ResetEmail);

            // Assert 
            Assert.AreEqual(LoginValidation.ResetPasswordConfirmation, "Your login details have been sent to your email address.");

        }

    }
}
