﻿using AussiepayTest;
using AussiepayTest.Employee;
using AussiepayTest.SignUp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignUpTest
{
    public class SignUpTest : Base
    {
        private string FamilyName = Employee.FamilyNam;
        private string GivenName = Employee.GivenNam;
        private string MasterCard = "5105105105105100";
        private string VisaCard = "4444333322221111";
        private string VisaType = "Visa";
        private string MasterType = "Mastercard";   


        [Test]
        [Obsolete]
        public void CanSignUPWithMasterCard()
        {
            string Email = FamilyName + "." + GivenName + DateTime.Now.ToString("hhmmss") + "@gmail.com";
            string EmployerName = "Test Company " + DateTime.Now.ToString("hhmmss");

            // Act
            SignUp.NavigateToSignUpPage();
            SignUp.ProvideAccountInformation(FamilyName, GivenName, Email, EmployerName);
            SignUp.ProvideCreditCardDetails(MasterCard, MasterType);
            SignUp.SubmitSignUp();

            // Assert
            //Assert.AreEqual(SignUpValidation.EmailVarification, Email);
        }
    }

}
