﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AussiepayTest
{
        public class Base
        {

            [OneTimeSetUp]
            public void Initialise()
            {               

                Driver.Initialize();
            }

            [TearDown]
            public void TestRunner()
            {
                
                Driver.Instance.Manage().Cookies.DeleteAllCookies();
            }

            [OneTimeTearDown]
            public void GetResult()
            {               
                Driver.Close();
                
            }

        }
    }
