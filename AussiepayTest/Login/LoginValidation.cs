﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AussiepayTest.Login
{
    public class LoginValidation
    {
        public static string Title
        {
            get
            {
                var _title = Driver.Instance.FindElement(By.XPath(@"//*[@id='nav']/li[1]/a/p"));
                return _title.Text;
            }
        }

        public static string Warning
        {
            get
            {
                var _warning = Driver.Instance.FindElement(By.XPath(@"//*[@id='Login']/div[2]/div"));
                return _warning.Text;
            }
        }

        public static string ResetPasswordConfirmation
        {
            get
            {
                var _warning = Driver.Instance.FindElement(By.XPath(@"//*[@id='Login']/div[2]/div"));
                return _warning.Text;
            }
        }

        //
    }
}
