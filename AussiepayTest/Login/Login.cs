﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AussiepayTest.Login
{
    public class Login
    {
        [Obsolete]
        public static void _Login(string email, string password)
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(60));

            try
            {
                Driver.Instance.Navigate().GoToUrl("https://saasuat.epayroll.com.au/");

                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//input[@id='UserName']")));

                /** The above was is called to confirm the page is loaded before it executes the next command */

                Driver.Instance.FindElement(By.Id("UserName")).SendKeys(email);
                Driver.Instance.FindElement(By.XPath(@"//button[contains(text(),'Next')]")).Click();
                Driver.Instance.FindElement(By.Id("Password")).SendKeys(password);
                Driver.Instance.FindElement(By.XPath(@"//*[@id='Login']/div[4]/button[1]")).Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());

            }

        }


        [Obsolete]
        public static void ResetPassword(string email)
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(60));

            try
            {
                Driver.Instance.Navigate().GoToUrl("https://saasuat.epayroll.com.au/");

                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//input[@id='UserName']")));

                /** The above was is called to confirm the page is loaded before it executes the next command */

                Driver.Instance.FindElement(By.Id("UserName")).SendKeys(email);
                Driver.Instance.FindElement(By.XPath(@"//button[contains(text(),'Next')]")).Click();
                Driver.Instance.FindElement(By.XPath(@"//*[@id='forgot-password']")).Click();
                Driver.Instance.FindElement(By.XPath(@"//*[@id='forgot-password-modal']/div/div/div[3]/button[2]")).Click();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());

            }

        }
       
    }
}
