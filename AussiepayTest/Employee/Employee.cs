﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AussiepayTest.Employee
{
    public class Employee
    {
        private static string FamilyName()
        {
            var ran = new Random();
            string[] fName =
                {"Poudel", "Devireddy" , "Agarwal" , "Collins" , "Morrely" , "Raihan", "Mobarak" , "Digne" , "Benzama"
                            , "Adhikari" , "Ahmed" , "Badran" , "Maharjan"};
            return fName[ran.Next(fName.Length)];
        }

        private static string GivenName()
        {
            var ran = new Random();
            string[] gName =
                {"Amit", "Pavan" , "Arif" , "Mickle" , "Joyti" , "Shiktir", "Saurab" , "Rita" , "Shilpa" , "Anup" , "Samuel"
                            , "Hannan" , "Richard"};
            return gName[ran.Next(gName.Length)];
        }

        public static string FamilyNam
        {
            get
            {
                var name = FamilyName().ToString();
                return name;
            }
        }
        public static string GivenNam
        {
            get
            {
                var name = GivenName().ToString();
                return name;
            }
        }
    }
}
