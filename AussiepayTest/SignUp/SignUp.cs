﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AussiepayTest.SignUp
{
   public class SignUp
    {
        public static void NavigateToSignUpPage()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                Driver.Instance.Navigate().GoToUrl("https://saasuat.epayroll.com.au/Public/SignUp/SignUpFeature.aspx?aJ5muxcWHY93R3nYOl5FMg%3D%3D=HbsNLuZ1RrxmLwim%2B79UdA%3D%3D&2e9wfFbEUy%2FC5707MxpahQ%3D%3D=0BqOBiSv5wVlrAUR0yheig%3D%3D&");

                wait.Until(drv => drv.FindElement(By.Id("phMain_givenname")));

            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        public static void ProvideAccountInformation(string FamilyName, string GivenName, string Email, string EmployerName)
        {
            try
            {
                Driver.Instance.FindElement(By.Id("phMain_givenname")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_givenname")).SendKeys(GivenName);
                Driver.Instance.FindElement(By.Id("phMain_surname")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_surname")).SendKeys(FamilyName);
                Driver.Instance.FindElement(By.Id("phMain_txtEmail")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_txtEmail")).SendKeys(Email);
                Driver.Instance.FindElement(By.Id("phMain_txtRetypeEmail1")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_txtRetypeEmail1")).SendKeys(Email);
                Driver.Instance.FindElement(By.Id("phMain_workphone")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_workphone")).SendKeys("0426397119");
                Driver.Instance.FindElement(By.Id("phMain_employerName")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_employerName")).SendKeys(EmployerName);
                Driver.Instance.FindElement(By.Id("phMain_txtEmployerNumber")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_txtEmployerNumber")).SendKeys("37093114286");
                SelectElement PaymentMethod = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_ddlEmployeePaymentMethod")));
                PaymentMethod.SelectByValue("1");
                System.Threading.Thread.Sleep(2000);
                SelectElement PayrollCycle = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_ddlPayrollCycle")));
                PayrollCycle.SelectByValue("1");
                System.Threading.Thread.Sleep(3000);
                Driver.Instance.FindElement(By.Id("phMain_payday")).Click();
                //Driver.Instance.FindElement(By.Id("phMain_payday")).SendKeys(DateTime.Now.ToString("dd/mm/YY"));
                Driver.Instance.FindElement(By.Id("phMain_txtDefaultHours")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_txtDefaultHours")).SendKeys("40");
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        public static void ProvideCreditCardDetails(string cardnumber, string Type)
        {
            try
            {
                SelectElement CardType = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_ddlCreditCardType")));
                CardType.SelectByValue(Type);
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardNumber")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardNumber")).SendKeys(cardnumber);
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardName")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardName")).SendKeys("TestCard");
                System.Threading.Thread.Sleep(2000);
                SelectElement Month = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_ddlCreditCardExpiryDate_ddlMonth")));
                Month.SelectByValue("6");
                System.Threading.Thread.Sleep(2000);
                SelectElement Year = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_ddlCreditCardExpiryDate_ddlYear")));
                Year.SelectByValue("2023");
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardSecurityCode")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardSecurityCode")).SendKeys("785");
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.Id("phMain_CheckBox1")).Click();


            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        [Obsolete]
        public static void SubmitSignUp()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {

                Driver.Instance.FindElement(By.Id("phMain_buttonRegister")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id("phMain_EmailVerification")));
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        public static void ProvideDirectDebitDetails()
        {
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//span[contains(text(),'Direct Debit')]")).Click();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_directDebit_txtAccountName")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_directDebit_txtAccountName")).SendKeys("TestCard");
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardSecurityCode")).Clear();
                Driver.Instance.FindElement(By.Id("phMain_paymentProcess_creditCard_txtCreditCardSecurityCode")).SendKeys("062191");
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.Id("phMain_CheckBox1")).Click();

            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }
    }
}
